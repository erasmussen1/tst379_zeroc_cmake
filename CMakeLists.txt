cmake_minimum_required(VERSION 3.16)

set(CMAKE_SYSTEM_NAME Linux)

project(DemoIceProject VERSION 1.01.001)
set(Package_maintainer "Egil Rasmussen")

set(CMAKE_C_STANDARD 99)
set(CMAKE_CXX_STANDARD 17)

option(RELEASE_BUILD "Release build " OFF)
option(BUILD_UNITTEST "Build unittests " OFF)
option(ENABLE_CCACHE "Enable ccache" OFF)
option(BUILD_SHARED  "Build as shared library" OFF)
option(ENABLE_SANITIZER "Enable address sanitizer" OFF)
option(BUILD_BENCHMARK "Build benchmarking test" OFF)

# Switch to enable a more verbose output for the gmake useful for debugging)
set(CMAKE_VERBOSE_MAKEFILE OFF)

set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)

# Create json meta data for Visual Studio Code, clang-tidy, code complete and etc
set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

if (Ice_FOUND)
    message(WARNING "  === Ice library is found === ")
endif()

if (BUILD_UNITTEST)
    enable_testing()
endif()

if (BUILD_SHARED)
    set(BUILD_TYPE "SHARED")
else()
    set(BUILD_TYPE "STATIC")
endif()

if (BUILD_BENCHMARK)
    find_package(benchmark REQUIRED)
endif()

# Get the current working branch from git
execute_process(
    COMMAND git rev-parse --abbrev-ref HEAD
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_BRANCH
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Get the latest abbreviated commit hash of the working branch
execute_process(
    COMMAND git log -1 --format=%h
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_COMMIT_HASH
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

configure_file(
    ${CMAKE_SOURCE_DIR}/ProjectVersion.h.in
    ${CMAKE_BINARY_DIR}/ProjectVersion.h
)

include_directories(
    ${CMAKE_BINARY_DIR}
)

# Common compiler options for debug and release builds
#
# Note: Special compiler options e.g. in a library should
# be specified in the cmake file for that library
#
set(flag " ")
set(flag "${flag} -Wpointer-arith")
set(flag "${flag} -Wno-shadow ")
set(flag "${flag} -Wcast-qual")
set(flag "${flag} -Wsign-compare")
set(flag "${flag} -Wunused-variable")
set(flag "${flag} -Wunused-value")
set(flag "${flag} -Wunused-label")
set(flag "${flag} -Wparentheses")
set(flag "${flag} -Wreturn-type")
set(flag "${flag} -Wno-unused-function")

# C only flags
set(flag_c "${flag_c} -Wmissing-prototypes")

# CPP only flags
set(flag_cpp "${flag_cpp} -Wreorder")

if(RELEASE_BUILD)
  message(WARNING "  === Release build === ")
  set(flag "-O3 -DNDEBUG")
  set(flag "${flag} -Wno-shift-count-overflow")
else()
  message(WARNING "  === Debug build === ")
  set(CMAKE_BUILD_TYPE Debug)

  set(flag "${flag} -Wall")
  set(flag "${flag} -g ")
  set(flag "${flag} -O0")
  set(flag "${flag} -Werror")

  if(ENABLE_SANITIZER)
    message(WARNING "  === sanitizer is enabled === ")
    # http://clang.llvm.org/docs/AddressSanitizer.html
    #
    # set(flag "${flag} -fsanitize=leak -fno-omit-frame-pointer")
    # set(flag "${flag} -fsanitize=thread -fno-omit-frame-pointer")
    set(flag "${flag} -fsanitize=address -fno-omit-frame-pointer ")
  endif()

  if(ENABLE_CCACHE)
    message(WARNING "  === ccache is enabled === ")
    find_program(CCACHE_FOUND ccache)

    if(CCACHE_FOUND)
      message(WARNING "  === ccache found === ")
      set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
      set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
    endif(CCACHE_FOUND)
  endif()

endif()

set(flag "${flag} -DICE_CPP11_MAPPING")

set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   ${flag} ${flag_c} " CACHE STRING "compile flags" FORCE)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${flag} ${flag_cpp} " CACHE STRING "compile flags" FORCE)

set(INSTALL_INCLUDE_DIR "${CMAKE_INSTALL_PREFIX}/include")
set(INSTALL_LIB_DIR "${CMAKE_INSTALL_PREFIX}/lib")
set(INSTALL_BIN_DIR "${CMAKE_INSTALL_PREFIX}/bin")
set(INSTALL_TEST_DIR "${CMAKE_INSTALL_PREFIX}/test")

add_subdirectory(lib)
add_subdirectory(app)

# This section generates a install package
include(InstallRequiredSystemLibraries)

set(CPACK_PACKAGING_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX})

set(CPACK_GENERATOR "RPM")
if(CMAKE_BUILD_TYPE STREQUAL "Debug")
  set(CPACK_RPM_SPEC_INSTALL_POST "/bin/true")
endif()

message(WARNING "  === Building Debian package === ")
set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER ${Package_maintainer})
#set(CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA ${CMAKE_CURRENT_SOURCE_DIR}/postinst)

set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
set(CPACK_PACKAGE_VERSION_MAJOR "${PROJECT_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${PROJECT_VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${PROJECT_VERSION_PATCH}.${GIT_COMMIT_HASH}")

# Build a CPack driven installer package
include (CPack)

