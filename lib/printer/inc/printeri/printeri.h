#ifndef PRINTERI_H__
#define PRINTERI_H__

#include <Ice/Ice.h>
#include <printer.h>

class PrinterI : public Demo::Printer {
public:
    virtual void printString(std::string s, const Ice::Current&) override;
};

#endif

