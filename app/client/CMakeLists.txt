project(client)

add_executable(${PROJECT_NAME} client.cpp)

target_link_libraries(${PROJECT_NAME} LINK_PUBLIC printer)

install(TARGETS ${PROJECT_NAME} DESTINATION ${INSTALL_BIN_DIR} )
