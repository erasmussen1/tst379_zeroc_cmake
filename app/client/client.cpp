//
// c++ -I. -DICE_CPP11_MAPPING -c Printer.cpp Client.cpp
// c++ -o client Printer.o Client.o -lIce++11
//

#include <stdexcept>

#include <Ice/Ice.h>
#include <printer.h>
#include <printeri/printeri.h>



int main(int argc, char* argv[]) {
    try {
        Ice::CommunicatorHolder ich(argc, argv);
        auto base = ich->stringToProxy("SimplePrinter:default -p 10000");

        auto printer = Ice::checkedCast<Demo::PrinterPrx>(base);
        if (!printer) {
            throw std::runtime_error("Invalid proxy");
        }

        printer->printString("Hello World!");

    } catch (const std::exception& e) {
        std::cerr << e.what() << "\n";
        return 1;
    }

    return 0;
}
