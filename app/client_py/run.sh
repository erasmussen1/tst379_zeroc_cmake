#!/bin/bash
#
# If Ice python module doesn't exist, install following
#
#  sudo apt-key adv --keyserver keyserver.ubuntu.com --recv B6391CB2CFBA643D
#
#  sudo apt-get install python3-zeroc-ice

[[ ! -e client.py ]] && die "ERR: Client doesn't exist!"
if [[ ! -d Demo ]]
then
    echo "ERR: Ice generated files do not exist!"
    slice2py ../../lib/printer/printer.ice --output-dir $(pwd)
fi

python3 client.py


